package com.luisbenvenuto;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.luisbenvenuto.config.PasswordEncoder;

@SpringBootApplication
public class NotasApplication implements CommandLineRunner {

	@Autowired
	private PasswordEncoder passwordEncoder;

	public static void main(String[] args) {
		SpringApplication.run(NotasApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {

		// Genera 2 password codificados por medio del passwordEncoder a artir de los
		// caracteres 12345
		String password = "12345";

		for (int i = 0; i < 2; i++) {
			String bcryptPassword = passwordEncoder.getPasswordEncoder().encode(password);
			System.out.println(bcryptPassword);
		}

	}

}
