package com.luisbenvenuto.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.luisbenvenuto.entity.Alumno;

public interface AlumnoRepository extends JpaRepository<Alumno, Long> {

	public List<Alumno> findByUsuario(String usuario);

}
