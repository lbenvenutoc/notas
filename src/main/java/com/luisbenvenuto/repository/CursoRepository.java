package com.luisbenvenuto.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.luisbenvenuto.entity.Curso;

public interface CursoRepository extends JpaRepository<Curso, Long> {
	
	public List<Curso> findByCodigoAlumno(Long codigoAlumno);

}
