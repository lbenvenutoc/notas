package com.luisbenvenuto.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import lombok.Getter;

@Configuration
@Getter
public class PasswordEncoder {

	private BCryptPasswordEncoder passwordEncoder;

	public PasswordEncoder() {
		passwordEncoder = new BCryptPasswordEncoder();
	}

}
