package com.luisbenvenuto.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

import com.luisbenvenuto.dto.CursoDto;
import com.luisbenvenuto.entity.Curso;

@Mapper(componentModel = "spring")
public interface CursoMapper {
	
	CursoDto cursoToCursoDto(Curso curso);

	@Mappings({ @Mapping(source = "codigoAlumno", target = "codigoAlumno"),
			@Mapping(source = "descripcion", target = "descripcion") })
	Curso cursoDtoToCurso(CursoDto cursoDto);

}
