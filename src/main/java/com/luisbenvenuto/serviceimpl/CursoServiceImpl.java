package com.luisbenvenuto.serviceimpl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.luisbenvenuto.dto.CursoDto;
import com.luisbenvenuto.entity.Curso;
import com.luisbenvenuto.mapper.CursoMapper;
import com.luisbenvenuto.repository.CursoRepository;
import com.luisbenvenuto.service.CursoService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class CursoServiceImpl implements CursoService {

	@Autowired
	private CursoRepository cursoRepository;

	@Autowired
	private CursoMapper cursoMapper;

	@Override
	public CursoDto registrarCurso(CursoDto cursoDto) {
		log.info("registrarCurso");
		log.info("cursoDto: " + cursoDto.toString());
		Curso cursoJpa = cursoRepository.save(cursoMapper.cursoDtoToCurso(cursoDto));
		if (cursoJpa != null) {
			return cursoMapper.cursoToCursoDto(cursoJpa);
		}
		return null;
	}

	@Override
	public List<CursoDto> listarCurso(String codigoAlumno) {
		log.info("listarCurso");
		log.info("codigoAlumno: " + codigoAlumno);
		List<Curso> lstCursoJpa = cursoRepository.findByCodigoAlumno(Long.parseLong(codigoAlumno));
		List<CursoDto> lstCursoDto = new ArrayList<CursoDto>();
		if (!lstCursoJpa.isEmpty()) {
			for (Curso cursoJpa : lstCursoJpa) {
				lstCursoDto.add(cursoMapper.cursoToCursoDto(cursoJpa));
			}
		}
		return lstCursoDto;
	}

}
