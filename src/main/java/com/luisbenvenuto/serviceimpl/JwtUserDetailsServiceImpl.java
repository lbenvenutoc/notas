package com.luisbenvenuto.serviceimpl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.luisbenvenuto.entity.Alumno;
import com.luisbenvenuto.repository.AlumnoRepository;

@Service
public class JwtUserDetailsServiceImpl implements UserDetailsService {

	@Autowired
	private AlumnoRepository alumnoRepository;

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		List<Alumno> lstAlumnoJpa = alumnoRepository.findByUsuario(username);
		if (!lstAlumnoJpa.isEmpty()) {
			List<GrantedAuthority> authorities = new ArrayList<>();
			if (lstAlumnoJpa.get(0).getUsuario().equals(username)) {
				authorities.add(new SimpleGrantedAuthority(lstAlumnoJpa.get(0).getRol()));
				return new User(lstAlumnoJpa.get(0).getUsuario(), lstAlumnoJpa.get(0).getClave(), authorities);
			} else {
				throw new UsernameNotFoundException("User not found with username: " + username);
			}
		} else {
			throw new UsernameNotFoundException("User not found with username: " + username);
		}

	}

}
