package com.luisbenvenuto.service;

import java.util.List;

import com.luisbenvenuto.dto.CursoDto;

public interface CursoService {

	public CursoDto registrarCurso(CursoDto cursoDto);

	public List<CursoDto> listarCurso(String codigoAlumno);

}
