package com.luisbenvenuto.dto;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class JwtResponseDto implements Serializable {

	private static final long serialVersionUID = -5078657683944652680L;
	private final String jwttoken;

}
