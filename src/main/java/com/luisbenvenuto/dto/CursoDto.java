package com.luisbenvenuto.dto;

import java.io.Serializable;

import javax.validation.constraints.NotEmpty;

import lombok.Data;

@Data
public class CursoDto implements Serializable {

	private static final long serialVersionUID = -6303546151545675302L;
	private String codigo;

	@NotEmpty(message = "no puede estar vacio")
	private String descripcion;

	@NotEmpty(message = "no puede estar vacio")
	private String nota;

	@NotEmpty(message = "no puede estar vacio")
	private String codigoAlumno;

}
