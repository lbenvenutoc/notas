package com.luisbenvenuto.dto;

import java.io.Serializable;

import lombok.Data;

@Data
public class JwtRequestDto implements Serializable {
	
	private static final long serialVersionUID = -3378773626908860072L;
	private String username;
	private String password;
	
}
	
	
