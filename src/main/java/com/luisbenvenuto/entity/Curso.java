package com.luisbenvenuto.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Table(name = "tb_curso")
@Data
public class Curso implements Serializable {

	private static final long serialVersionUID = 6614162735633570427L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "codigo")
	private Long codigo;

	@Column(name = "descripcion")
	private String descripcion;

	@Column(name = "nota")
	private Double nota;

	@Column(name = "codigo_alumno")
	private Long codigoAlumno;

}
