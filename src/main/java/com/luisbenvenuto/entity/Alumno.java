package com.luisbenvenuto.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Table(name = "tb_alumno")
@Data
public class Alumno implements Serializable {

	private static final long serialVersionUID = -3116556068763823415L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "codigo")
	private Long codigo;
	
	@Column(name = "usuario")
	private String usuario;

	@Column(name = "nombre")
	private String nombre;
	
	@Column(name = "clave")
	private String clave;
	
	@Column(name = "rol")
	private String rol;

	@OneToMany(fetch = FetchType.LAZY)
	@JoinColumn(name = "codigo_alumno", referencedColumnName = "codigo")
	private List<Curso> lstCuso;

}
