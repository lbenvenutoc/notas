package com.luisbenvenuto.restcontroller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.luisbenvenuto.dto.CursoDto;
import com.luisbenvenuto.service.CursoService;

@RequestMapping("/alumnos")
@RestController
public class AlumnoRestController {

	@Autowired
	private CursoService cursoService;

	@Secured("ROLE_ALUMNO")
	@GetMapping("/{codigoAlumno}/notas/")
	public List<CursoDto> listarNotasPorAlumno(@PathVariable("codigoAlumno") String codigoAlumno) {
		List<CursoDto> lstCursoDto = cursoService.listarCurso(codigoAlumno);
		return lstCursoDto;
	}

}
