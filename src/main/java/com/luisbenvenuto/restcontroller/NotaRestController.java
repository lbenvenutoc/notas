package com.luisbenvenuto.restcontroller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.luisbenvenuto.dto.CursoDto;
import com.luisbenvenuto.service.CursoService;

@RequestMapping("/notas")
@RestController
public class NotaRestController {

	@Autowired
	private CursoService cursoService;
	
	@Secured("ROLE_ADMIN")
	@PostMapping("/")
	public ResponseEntity<?> grabarNota(@Valid @RequestBody CursoDto cursoDto, BindingResult result) {
		Map<String, Object> response = new HashMap<>();
		CursoDto cursoResultadoDto = null;
		if (result.hasErrors()) {
			List<String> errors = result.getFieldErrors().stream()
					.map(err -> "El campo '" + err.getField() + "' " + err.getDefaultMessage())
					.collect(Collectors.toList());

			response.put("errors", errors);
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.BAD_REQUEST);
		}
		try {
			cursoResultadoDto = cursoService.registrarCurso(cursoDto);
			if (cursoResultadoDto != null) {
				response.put("mensaje", "La nota ha sido registrada con éxito!");
				response.put("curso", cursoResultadoDto);
			}
		} catch (DataAccessException e) {
			response.put("mensaje", "Error al realizar el insert en la base de datos");
			response.put("error", e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage()));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}

		return new ResponseEntity<Map<String, Object>>(response, HttpStatus.CREATED);
	}

}
